package cz.uhk.fim.todolist.gui;

import cz.uhk.fim.todolist.model.TodoItem;
import cz.uhk.fim.todolist.model.TodoList;
import cz.uhk.fim.todolist.model.TodoTableModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


/*
* Check box
* Validace
* */


public class MainFrame extends JFrame {
    private JTable table;
    private TodoList todoList;
    private TodoTableModel model;

    public MainFrame() {
        setVisible(true);
        init();
    }

    private void init() {
        setTitle("Todo list.");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(new Dimension(500, 500));
        setLocationRelativeTo(null);

        todoList = new TodoList();
        model = new TodoTableModel();
        model.setList(todoList);

        initControlPanel();
        initContentPanel();
    }

    private void initContentPanel() {
        JPanel contentPanel = new JPanel(new BorderLayout());
        table = new JTable();
        table.setModel(model);
        contentPanel.add(new JScrollPane(table));
        add(contentPanel, BorderLayout.CENTER);

        table.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (table.getEditingColumn() == 1) {
                    if ((boolean) table.getValueAt(table.getEditingRow(), 1)) {
                        DateTimeFormatter formater = DateTimeFormatter.ofPattern("dd. MM. yyyy - HH:mm:ss");
                        LocalDateTime now = LocalDateTime.now();
                        table.setValueAt(formater.format(now), table.getEditingRow(), 2);
                    } else {
                        table.setValueAt("", table.getEditingRow(), 2);
                    }
                }
            }
        });
    }

    private void initControlPanel() {
        JPanel controlPanel = new JPanel(new BorderLayout());
        JPanel formPanel = new JPanel(new BorderLayout());

        JLabel lblAddTodo = new JLabel("Todo name: ");
        JTextField txtAddTodo = new JTextField();
        JButton btnAddTodo = new JButton("Add");

        btnAddTodo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!txtAddTodo.getText().trim().isEmpty()) {
                    todoList.add(new TodoItem(txtAddTodo.getText()));
                    txtAddTodo.setText("");
                    model.setList(todoList);
                } else {
                    JOptionPane.showMessageDialog(null, "Zadejte text!", "Chyba", JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        formPanel.add(lblAddTodo, BorderLayout.WEST);
        formPanel.add(txtAddTodo, BorderLayout.CENTER);
        formPanel.add(btnAddTodo, BorderLayout.EAST);

        controlPanel.add(formPanel, BorderLayout.NORTH);

        add(controlPanel, BorderLayout.NORTH);
    }
}

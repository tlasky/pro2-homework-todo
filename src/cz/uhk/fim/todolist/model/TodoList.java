package cz.uhk.fim.todolist.model;

import java.util.ArrayList;
import java.util.List;

public class TodoList {
    private List<TodoItem> itemList = new ArrayList();

    public int getItemCount() {
        return itemList.size();
    }

    public void add(TodoItem item) {
        itemList.add(item);
    }

    public TodoItem get(int index) {
        return itemList.get(index);
    }

    public void setCompleted(int index, boolean value) {
        itemList.get(index).setCompleted(value);
    }

    public void changeComplete(int index) {
        TodoItem item = itemList.get(index);
        item.setCompleted(!item.isCompleted());
    }
}

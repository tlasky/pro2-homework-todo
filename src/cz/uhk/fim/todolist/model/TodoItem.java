package cz.uhk.fim.todolist.model;

public class TodoItem {
    private String title;
    private boolean completed;
    private String dateTime;

    public TodoItem() {
        this.title = "";
        this.completed = false;
        this.dateTime = "";
    }

    public TodoItem(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
